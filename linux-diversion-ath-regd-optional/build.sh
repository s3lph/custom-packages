#!/bin/bash
#
# Build the current linux kernel with a patch permitting ath10k WiFi
# cards with no burned-in regdom to be used as 5 GHz APs again.
# linux-5.6 "sanitizes the regdom value 0x00 (= unset) to
# 0x64 (= worldwide, most restricted), which works fine in STA mode, but
# does not permit setting a more relaxed regdom in AP mode, breaking
# 5 GHz WiFi.
#
# Build instructions from
# https://github.com/twisteroidambassador/arch-linux-ath-user-regd/issues/1
#
# Revised kernel patch from
# https://github.com/CodePhase/patch-atheros-regdom

set -xe

ROOT="$(pwd)"
export BUILDDIR="${ROOT}/build"
export SRCDIR="${ROOT}/build/srcdir"
export PKGDIR="${ROOT}/build/pkgdir"
export PKGDIR_META="${ROOT}/build/pkgdir_meta"
mkdir -p "${SRCDIR}" "${PKGDIR}" "${PKGDIR_META}"

# Add deb-src lines missing in the docker:bullseye image
sed 's/^Types: deb$/Types: deb-src/g' /etc/apt/sources.list.d/debian.sources > /etc/apt/sources.list.d/10-deb-src.sources

apt update
apt build-dep -y linux

# Get kernel version from orig filename
cd "${SRCDIR}"
apt source linux
export LINUX_VERSION=$(ls -1 linux_*.orig.tar.xz | sed -re 's/^linux_(.*).orig.tar.xz$/\1/g' | head -1)
export LINUX_REVISION=$(ls -1 linux_*.dsc | sed -re 's/^linux_(.*).dsc$/\1/g' | head -1)
# Get debian package version from source's debian/control (contains lots
# of binary packages, pick linux-headers-{version}-common, because it's
# the first one.
export DEB_VERSION=$(grep '^Package: linux-headers-.*-common$' "linux-${LINUX_VERSION}/debian/control" \
			 | sed -re 's/^Package: linux-headers-(.*)-common$/\1/g')
# Download the already built binary package and extract the kernel config
mkdir -p "${SRCDIR}/unpack/image" "${SRCDIR}/unpack/headers"
cd "${SRCDIR}/unpack/image"
apt download "linux-image-${DEB_VERSION}-amd64"
ar xf "linux-image-${DEB_VERSION}-amd64_${LINUX_REVISION}_amd64.deb" ./data.tar.xz
tar xf data.tar.xz ./boot/config-${DEB_VERSION}-amd64
# Download the already built headers package and extract symvers
cd "${SRCDIR}/unpack/headers"
apt download "linux-headers-${DEB_VERSION}-amd64"
ar xf "linux-headers-${DEB_VERSION}-amd64_${LINUX_REVISION}_amd64.deb" ./data.tar.xz
tar xf data.tar.xz "./usr/src/linux-headers-${DEB_VERSION}-amd64/Module.symvers"

# Apply the patch and copy kernel config and symvers
cd "${SRCDIR}/linux-${LINUX_VERSION}"
make clean mrproper
patch -p1 < "${ROOT}/ath_regd.patch"
cp "${SRCDIR}/unpack/image/boot/config-${DEB_VERSION}-amd64" "${SRCDIR}/linux-${LINUX_VERSION}/.config"
cp "${SRCDIR}/unpack/headers/usr/src/linux-headers-${DEB_VERSION}-amd64/Module.symvers" "${SRCDIR}/linux-${LINUX_VERSION}/Module.symvers"
# Set the new configuration flag
./scripts/config --set-val ATH_USER_REGD y
make oldconfig

# Build and compress the "ath" kernel module
make prepare modules_prepare scripts
make M=drivers/net/wireless/ath

# This will be used as timestamp in the "ar" archives
export SOURCE_DATE_EPOCH=$(date +%s -r "${SRCDIR}/unpack/image/boot/config-${DEB_VERSION}-amd64")
# Prepare the diversion package
rsync -a "${ROOT}/debian/" "${PKGDIR}/DEBIAN/"
mkdir -p \
    "${PKGDIR}/lib/modules/${DEB_VERSION}-amd64/kernel/drivers/net/wireless/ath" \
    "${PKGDIR}/usr/share/doc/linux-diversion-${DEB_VERSION}-ath-regd-optional"
cp "${SRCDIR}/linux-${LINUX_VERSION}/drivers/net/wireless/ath/ath.ko" \
    "${PKGDIR}/lib/modules/${DEB_VERSION}-amd64/kernel/drivers/net/wireless/ath/ath.ko"
# Insert version numbers into control files
for FILE in $(echo "${PKGDIR}"/DEBIAN/*); do
    sed -re "s/__DEB_VERSION__/${DEB_VERSION}/g" -i "${FILE}"
    sed -re "s/__LINUX_VERSION__/${LINUX_VERSION}/g" -i "${FILE}"
    sed -re "s/__LINUX_REVISION__/${LINUX_REVISION}/g" -i "${FILE}"
done
mv "${PKGDIR}/DEBIAN/changelog" "${PKGDIR}/usr/share/doc/linux-diversion-${DEB_VERSION}-ath-regd-optional/changelog.Debian"
mv "${PKGDIR}/DEBIAN/copyright" "${PKGDIR}/usr/share/doc/linux-diversion-${DEB_VERSION}-ath-regd-optional/"
gzip -9n "${PKGDIR}/usr/share/doc/linux-diversion-${DEB_VERSION}-ath-regd-optional/changelog.Debian"
chown 0:0 -R "${PKGDIR}"
find "${PKGDIR}" -type f -exec chmod 0644 {} \;
find "${PKGDIR}" -type d -exec chmod 0755 {} \;
chmod 0755 "${PKGDIR}/DEBIAN/preinst" "${PKGDIR}/DEBIAN/postinst" "${PKGDIR}/DEBIAN/postrm"
# change mtime to the original package's config file
find "${PKGDIR}" -exec touch -m -r "${SRCDIR}/unpack/image/boot/config-${DEB_VERSION}-amd64" {} \;
# Build the diversion package
cd "${BUILDDIR}"
dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"

# Prepare the meta package
rsync -a "${ROOT}/debian.meta/" "${PKGDIR_META}/DEBIAN/"
mkdir -p \
    "${PKGDIR_META}/usr/share/doc/linux-diversion-ath-regd-optional"
# Insert version numbers into control files
for FILE in $(echo "${PKGDIR_META}"/DEBIAN/*); do
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${FILE}"
    sed -re "s/__DEB_VERSION__/${DEB_VERSION}/g" -i "${FILE}"
    sed -re "s/__LINUX_VERSION__/${LINUX_VERSION}/g" -i "${FILE}"
    sed -re "s/__LINUX_REVISION__/${LINUX_REVISION}/g" -i "${FILE}"
done
mv "${PKGDIR_META}/DEBIAN/changelog" "${PKGDIR_META}/usr/share/doc/linux-diversion-ath-regd-optional/changelog.Debian"
mv "${PKGDIR_META}/DEBIAN/copyright" "${PKGDIR_META}/usr/share/doc/linux-diversion-ath-regd-optional/"
gzip -9n "${PKGDIR_META}/usr/share/doc/linux-diversion-ath-regd-optional/changelog.Debian"
chown 0:0 -R "${PKGDIR_META}"
find "${PKGDIR_META}" -type f -exec chmod 0644 {} \;
find "${PKGDIR_META}" -type d -exec chmod 0755 {} \;
# change mtime to the original package's config file
find "${PKGDIR_META}" -exec touch -m -r "${SRCDIR}/unpack/image/boot/config-${DEB_VERSION}-amd64" {} \;
# Build the diversion package
cd "${BUILDDIR}"
dpkg-deb --build "${PKGDIR_META}" "${BUILDDIR}"
