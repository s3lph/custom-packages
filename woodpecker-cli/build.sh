#!/bin/bash

set -exo pipefail

N_RELEASES=1
API_URL=https://api.github.com/repos/woodpecker-ci/woodpecker/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false ) | "\(.tag_name[1:]) \(.published_at) \(.assets[] | select( .name=="woodpecker-cli_linux_amd64.tar.gz" ).browser_download_url)"'

ROOT=$(pwd)
BUILDDIR=${ROOT}/build/
SRCDIR=${ROOT}/build/srcdir
PKGDIR=${ROOT}/build/pkgdir

function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "woodpecker-cli-${VERSION}-linux-amd64.tar.gz"
    tar xf "woodpecker-cli-${VERSION}-linux-amd64.tar.gz"
}

function prepare() {
    chmod +x "${SRCDIR}/woodpecker-cli"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin"
    cp "${SRCDIR}/woodpecker-cli" "${PKGDIR}/usr/bin/woodpecker"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head "-${N_RELEASES}")
    export VERSION
    export ISODATE
    export URL
    mkdir -p "${SRCDIR}" "${PKGDIR}"
    fetch
    prepare
    package
}


build
