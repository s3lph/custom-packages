#!/bin/bash

. ../.skel/nextcloud-app/build-nextcloud-app

post_prepare() {
    mkdir -p "${PKGDIR}/lib/systemd/system/"
    cp "${ROOT}/nextcloud-preview-generator.service" "${PKGDIR}/lib/systemd/system/nextcloud-preview-generator.service"
    cp "${ROOT}/nextcloud-preview-generator.timer" "${PKGDIR}/lib/systemd/system/nextcloud-preview-generator.timer"
    github_changelog nextcloud/previewgenerator
}

postinst_configure_post() {
    deb-systemd-helper enable nextcloud-preview-generator.timer
}

build_nextcloud_app previewgenerator 27

