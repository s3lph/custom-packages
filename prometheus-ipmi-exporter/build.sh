#!/bin/bash

set -exo pipefail

. ../.skel/helpers.sh

PKGNAME=prometheus-ipmi-exporter
N_RELEASES=1
API_URL=https://api.github.com/repos/prometheus-community/ipmi_exporter/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false ) | "\(.tag_name[1:]) \(.published_at) \(.assets[] | select( .name|test(".*.linux-amd64.tar.gz$") ).browser_download_url)"'

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "ipmi-exporter-${VERSION}-linux-amd64.tar.gz"
    tar --strip-components=1 -xf "ipmi-exporter-${VERSION}-linux-amd64.tar.gz"
}

function prepare() {
    chmod +x "${SRCDIR}/ipmi_exporter"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/lib/systemd/system" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/default" \
          "${PKGDIR}/etc/prometheus/ipmi-exporter" \
          "${PKGDIR}/etc/sudoers.d" \
	  "${PKGDIR}/usr/share/doc/${PKGNAME}"
    cp "${SRCDIR}/ipmi_exporter" "${PKGDIR}/usr/bin/prometheus-ipmi-exporter"
    cp "${SRCDIR}/LICENSE" "${PKGDIR}/usr/share/doc/${PKGNAME}/copyright"
    cp "${ROOT}/prometheus-ipmi-exporter.service" "${PKGDIR}/lib/systemd/system/prometheus-ipmi-exporter.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/prometheus-ipmi-exporter.defaults" "${PKGDIR}/etc/default/prometheus-ipmi-exporter"
    cp "${ROOT}/prometheus-ipmi-exporter.sudoers" "${PKGDIR}/etc/sudoers.d/prometheus-ipmi-exporter"
    cp "${ROOT}/config.yaml" "${PKGDIR}/etc/prometheus/ipmi-exporter/config.yaml"
    chmod 0440 "${PKGDIR}/etc/sudoers.d/prometheus-ipmi-exporter"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    github_changelog prometheus-community/ipmi_exporter
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    while read VERSION ISODATE URL; do
        export VERSION
        export ISODATE
        export URL
        export BUILDDIR=${ROOT}/build
        export SRCDIR=${ROOT}/build/${VERSION}/srcdir
        export PKGDIR=${ROOT}/build/${VERSION}/pkgdir
        mkdir -p ${SRCDIR} ${PKGDIR}
        fetch
        prepare
        package
    done <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head "-${N_RELEASES}")
}
 

build
