#!/bin/bash

. ../.skel/nextcloud-app/build-nextcloud-app

export ARCHS=(amd64)

post_prepare() {
    github_changelog CollaboraOnline/richdocumentscode
}

post_chown() {
    chmod 0755 "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps/${APP}/collabora/Collabora_Online.AppImage"
}

build_nextcloud_app richdocumentscode 27 25
