#!/bin/bash

. ../.skel/nextcloud-app/build-nextcloud-app

post_prepare() {
    cat "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps/${APP}/CHANGELOG.md" | gzip -9n > "${PKGDIR}/usr/share/doc/${PKGNAME}/changelog.gz"
}

build_nextcloud_app gpoddersync 27

