#!/bin/bash

set -exo pipefail

API_URL=https://codeberg.org/api/v1/repos/forgejo/forgejo/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and (.tag_name|test("^v[0-9.-]+$")) ) | "\(.name[1:]) \(.published_at) \(.assets[] | select(.name|test(".*-linux-amd64.xz$")).browser_download_url )"'

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "forgejo-${VERSION}-linux-amd64.xz"
    xz --decompress "forgejo-${VERSION}-linux-amd64.xz"
}

function prepare() {
    chmod +x "${SRCDIR}/forgejo-${VERSION}-linux-amd64"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/gitea" \
          "${PKGDIR}/var/lib/gitea/custom" \
          "${PKGDIR}/var/lib/gitea/data" \
          "${PKGDIR}/var/lib/gitea/log" \
          "${PKGDIR}/var/lib/gitea/gitea-repositories" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/forgejo-${VERSION}-linux-amd64" "${PKGDIR}/usr/bin/forgejo"
    cp "${ROOT}/gitea.service" "${PKGDIR}/lib/systemd/system/gitea.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    cp "${ROOT}/app.ini" "${PKGDIR}/etc/gitea/app.ini"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    ln -s gitea.service "${PKGDIR}/lib/systemd/system/forgejo.service"
    ln -s gitea "${PKGDIR}/etc/forgejo"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    # Replace Forgejo patch level separater - with . to be Debian versioning compatible, and add epoch number 2
    export VERSION="2:${VERSION/-/.}"
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
