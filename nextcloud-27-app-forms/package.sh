#!/bin/bash

. ../.skel/nextcloud-app/build-nextcloud-app

post_prepare() {
    github_changelog nextcloud/forms
}

build_nextcloud_app forms 27
