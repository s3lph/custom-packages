<?php

$CONFIG = array (
    'installed' => false,

    'trusted_domains' => [
        'nextcloud.example.com'
    ],
    'overwrite.cli.url' => 'http://nextcloud.example.com',
    'datadirectory' => '/var/lib/nextcloud/data',
    'logfile' => '/var/log/nextcloud/nextcloud.log',

    'mysql.utf8mb4' => true,

    'connectivity_check_domains' => ['localhost'],

    'appstoreenabled' => false,
    'apps_paths' => [
	[
		'path'=> '/usr/lib/nextcloud/nextcloud-apps',
		'url' => '/dist-apps',
		'writable' => false,
	]
    ],

);
