#!/bin/bash

set -exo pipefail

. ../.skel/helpers.sh

VERSION=27

URL="https://download.nextcloud.com/server/releases/latest-${VERSION}.tar.bz2"

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "nextcloud-${VERSION}.tar.bz2"
    tar -xf "nextcloud-${VERSION}.tar.bz2"
    patch --strip=1 --directory="${SRCDIR}" --ignore-whitespace < "${ROOT}/patches/01_isfairuse_userfacing_ui.patch"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/var/lib/nextcloud" \
          "${PKGDIR}/usr/lib/nextcloud" \
          "${PKGDIR}/var/lib/nextcloud/data" \
          "${PKGDIR}/lib/systemd/system" \
          "${PKGDIR}/var/log/nextcloud" \
          "${PKGDIR}/usr/local/bin" \
          "${PKGDIR}/etc/apache2/sites-available" \
          "${PKGDIR}/usr/share/doc/${PKGNAME}"
    VERSION=$(cat "${SRCDIR}/nextcloud/version.php" | grep "OC_VersionString" | cut -d"'" -f2)
    cp -r "${SRCDIR}/nextcloud" "${PKGDIR}/var/lib/nextcloud/webroot"
    mv "${PKGDIR}/var/lib/nextcloud/webroot/apps" "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps"
    cp "${ROOT}/config.php" "${PKGDIR}/var/lib/nextcloud/webroot/config/config.php"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/nextcloud-cron.service" "${ROOT}/nextcloud-cron.timer" "${PKGDIR}/lib/systemd/system/"
    cp "${ROOT}/nextcloud.site.conf" "${PKGDIR}/etc/apache2/sites-available/"
    cp "${ROOT}/occ.sh" "${PKGDIR}/usr/local/bin/occ"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    cp "${SRCDIR}/nextcloud/COPYING" "${PKGDIR}/usr/share/doc/${PKGNAME}/copyright"
    github_changelog nextcloud/server
    find "${PKGDIR}" -exec touch -m --reference "${SRCDIR}/nextcloud/version.php" {} \;
}

function _chown() {
    chown 0:0 -R "${PKGDIR}/"
    # www-data:www-data
    chown 33:33 -R \
	  "${PKGDIR}/var/lib/nextcloud" \
	  "${PKGDIR}/var/log/nextcloud" \
	  "${PKGDIR}/usr/lib/nextcloud"
    find "${PKGDIR}/var/lib/nextcloud/webroot" "${PKGDIR}/var/log/nextcloud" -type f -exec chmod 0640 {} \;
    find "${PKGDIR}/var/lib/nextcloud/webroot" "${PKGDIR}/var/log/nextcloud" -type d -exec chmod 0750 {} \;
    find "${PKGDIR}/usr/lib/nextcloud" -type f -exec chmod 0644 {} \;
    find "${PKGDIR}/usr/lib/nextcloud" -type d -exec chmod 0755 {} \;
    ln -sf "/var/lib/nextcloud/webroot/core" "${PKGDIR}/usr/lib/nextcloud/core"
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    export PKGNAME="nextcloud-${VERSION}"
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    _chown
    package
}
 

build
