#!/bin/bash

set -exo pipefail


ROOT=$(pwd)
function fetch() {
    apt install --assume-yes --no-install-recommends git npm
    cd "${SRCDIR}"
    git clone https://github.com/johni0702/mumble-web mumble-web
    cd mumble-web
    export VERSION=0.$(git rev-list --count HEAD)
    npm install
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
	  "${PKGDIR}/usr/share/mumble-web/html/" \
          "${PKGDIR}/etc/mumble-web/html/" \
	  "${PKGDIR}/etc/apache2/sites-available/"
    rsync -a "${SRCDIR}/mumble-web/dist/" "${PKGDIR}/usr/share/mumble-web/html/"
    chown root:root -R "${PKGDIR}/usr/share/mumble-web/html"
    mv "${PKGDIR}/usr/share/mumble-web/html/config.local.js" "${PKGDIR}/etc/mumble-web/html/config.js"
    cp "${ROOT}/mumble-web.site" "${PKGDIR}/etc/apache2/sites-available/mumble-web.site"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
