#!/bin/bash

set -xeo pipefail

ROOT=$(pwd)
PKGNAME=mediawiki-extension-auth-remoteuser
ARCH=all

MW_VERSION=REL1_39
MW_EXTENSION=Auth_remoteuser

MWED_URL="https://www.mediawiki.org/wiki/Special:ExtensionDistributor?extdistname=${MW_EXTENSION}&extdistversion=${MW_VERSION}"
PATTERN="https://extdist.wmflabs.org/dist/extensions/${MW_EXTENSION}-${MW_VERSION}-[^.]+.tar.gz"

function fetch() {
    URL="$(curl "${MWED_URL}" | grep -Eo "${PATTERN}" | head -1)"
    wget -O "${SRCDIR}/${PKGNAME}.tar.gz" "${URL}"
    tar -xf "${SRCDIR}/${PKGNAME}.tar.gz" -C "${SRCDIR}"
    export VERSION="$(cat "${SRCDIR}/${MW_EXTENSION}/extension.json" | jq -r .version)"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/var/lib/mediawiki/extensions" \
          "${PKGDIR}/usr/share/mediawiki-extensions"
    ln -sf "/usr/share/mediawiki-extensions/${MW_EXTENSION}" "${PKGDIR}/var/lib/mediawiki/extensions/${MW_EXTENSION}"
    rsync -a "${SRCDIR}/${MW_EXTENSION}/" "${PKGDIR}/usr/share/mediawiki-extensions/${MW_EXTENSION}/"
    cat > "${PKGDIR}/DEBIAN/control" <<EOF
Package: ${PKGNAME}
Version: ${VERSION}
Maintainer: ${MAINTAINER}
Section: web
Priority: optional
Architecture: ${ARCH}
Depends: mediawiki (>= 1.39)
Description: Mediawiki Extension ${MW_EXTENSION}
 See https://www.mediawiki.org/wiki/Extension:${MW_EXTENSION}
 for documentation.
EOF
    chown 0:0 -R "${PKGDIR}"
    find "${PKGDIR}" -exec touch -m --reference "${SRCDIR}/${MW_EXTENSION}/extension.json" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}

build
