#!/bin/bash

set -exo pipefail

PKGNAME=republik-feeder
API_URL=https://api.github.com/repos/maetthu/republik-feeder/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.tag_name[1:]) \(.published_at) \(.assets[] | select( .name|test(".*_linux_amd64.tar.gz$") ).browser_download_url)"'


ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    mkdir -p "${PKGNAME}-${VERSION}"
    wget "${URL}" --output-document "${PKGNAME}-${VERSION}.tar.gz"
    tar -x -C "${PKGNAME}-${VERSION}" -f "${PKGNAME}-${VERSION}.tar.gz"
    rm "${PKGNAME}-${VERSION}.tar.gz"
    cd "${PKGNAME}-${VERSION}"
}

function prepare() {
    chmod +x "${SRCDIR}/${PKGNAME}-${VERSION}/${PKGNAME}"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/default" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/${PKGNAME}-${VERSION}/republik-feeder" "${PKGDIR}/usr/bin/republik-feeder"
    cp "${ROOT}/republik-feeder.defaults" "${PKGDIR}/etc/default/republik-feeder"
    cp "${ROOT}/republik-feeder.service" "${PKGDIR}/lib/systemd/system/republik-feeder.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
