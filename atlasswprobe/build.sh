#!/bin/bash

set -exo pipefail

ROOT=$(pwd)

export BUILDDIR="${ROOT}/build"
export SRCDIR="${ROOT}/build/srcdir"
mkdir -p "${SRCDIR}"

cd "${SRCDIR}"
apt install --yes git tar fakeroot libssl-dev libcap2-bin autoconf automake libtool build-essential
git clone --recursive https://github.com/RIPE-NCC/ripe-atlas-software-probe

"${SRCDIR}/ripe-atlas-software-probe/build-config/debian/bin/make-deb"
mv atlasswprobe-*deb "${BUILDDIR}"
