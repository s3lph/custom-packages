#!/bin/bash

set -exo pipefail


ROOT=$(pwd)
function fetch() {
    apt install --assume-yes --no-install-recommends git golang-go
    cd "${SRCDIR}"
    git clone https://github.com/subzerobo/http-mqtt-bridge
    cd http-mqtt-bridge
    export VERSION=0.$(git rev-list --count HEAD)
    go get ./...
    go build -o http-mqtt-bridge
}

function prepare() {
    chmod +x "${SRCDIR}/http-mqtt-bridge/http-mqtt-bridge"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/default" \
          "${PKGDIR}/etc/apache2/sites-available" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/http-mqtt-bridge/http-mqtt-bridge" "${PKGDIR}/usr/bin/http-mqtt-bridge"
    cp "${ROOT}/http-mqtt-bridge.defaults" "${PKGDIR}/etc/default/http-mqtt-bridge"
    cp "${ROOT}/http-mqtt-bridge.service" "${PKGDIR}/lib/systemd/system/http-mqtt-bridge.service"
    cp "${ROOT}/http-mqtt-bridge.site" "${PKGDIR}/etc/apache2/sites-available/http-mqtt-bridge.site"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
