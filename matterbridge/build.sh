#!/bin/bash

set -exo pipefail

N_RELEASES=1
API_URL=https://api.github.com/repos/42wim/matterbridge/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.tag_name[1:]) \(.published_at) \(.assets[] | select( .name|test(".*-linux-64bit$") ).browser_download_url)"'

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "matterbridge-${VERSION}-linux-amd64"
}

function prepare() {
    chmod +x "${SRCDIR}/matterbridge-${VERSION}-linux-amd64"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/matterbridge" \
	  "${PKGDIR}/etc/default" \
          "${PKGDIR}/var/lib/matterbridge/upload" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/matterbridge-${VERSION}-linux-amd64" "${PKGDIR}/usr/bin/matterbridge"
    cp "${ROOT}/matterbridge.service" "${PKGDIR}/lib/systemd/system/matterbridge.service"
    cp "${ROOT}/matterbridge-upload-cleanup.service" "${PKGDIR}/lib/systemd/system/matterbridge-upload-cleanup.service"
    cp "${ROOT}/matterbridge-upload-cleanup.timer" "${PKGDIR}/lib/systemd/system/matterbridge-upload-cleanup.timer"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    cp "${ROOT}/matterbridge.toml.sample" "${PKGDIR}/etc/matterbridge/matterbridge.toml.sample"
    cp "${ROOT}/matterbridge.toml" "${PKGDIR}/etc/matterbridge/matterbridge.toml"
    cp "${ROOT}/matterbridge.default" "${PKGDIR}/etc/default/matterbridge"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    while read VERSION ISODATE URL; do
        export VERSION
        export ISODATE
        export URL
        export BUILDDIR=${ROOT}/build
        export SRCDIR=${ROOT}/build/${VERSION}/srcdir
        export PKGDIR=${ROOT}/build/${VERSION}/pkgdir
        mkdir -p ${SRCDIR} ${PKGDIR}
        fetch
        prepare
        package
    done <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head "-${N_RELEASES}")
}
 

build
