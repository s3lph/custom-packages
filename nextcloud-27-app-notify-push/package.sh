#!/bin/bash

. ../.skel/nextcloud-app/build-nextcloud-app

export ARCHS=(amd64 arm64 armhf)

function _ncarch() {
    case "$1" in
	amd64)
	    echo x86_64
	    ;;
	arm64)
	    echo aarch64
	    ;;
	armhf)
	    echo armv7
	    ;;
	*)
	    echo no such architecture
	    exit 1
    esac
}

function post_prepare() {
    NCARCH=$(_ncarch $ARCH)
    mkdir -p \
	  "${PKGDIR}/lib/systemd/system" \
	  "${PKGDIR}/etc/default"
    mv "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps/notify_push/bin/${NCARCH}/notify_push" \
       "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps/notify_push/bin/notify_push"
    find "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps/notify_push/bin/" -type d -mindepth 1 -maxdepth 1 -exec rm -rf {} \;
    cp "${ROOT}/nextcloud-notify-push.service" "${PKGDIR}/lib/systemd/system/nextcloud-notify-push.service"
    cp "${ROOT}/nextcloud-notify-push.default" "${PKGDIR}/etc/default/nextcloud-notify-push"
    cp "${SRCDIR}/notify_push/LICENSE" "${PKGDIR}/usr/share/doc/nextcloud-${NCMAJOR}-app-${APP//_/-}/copyright"
    echo '/etc/default/nextcloud-notify-push' > "${PKGDIR}/DEBIAN/conffiles"
    github_changelog nextcloud/notify_push
}

function post_chown() {
    chmod 0600 "${PKGDIR}/etc/default/nextcloud-notify-push"
    chmod 0755 "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps/notify_push/bin/notify_push"
}

function postinst_configure_post() {
    deb-systemd-helper enable nextcloud-notify-push.service
    deb-systemd-invoke restart nextcloud-notify-push.service
}

build_nextcloud_app notify_push 27
