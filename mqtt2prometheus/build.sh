#!/bin/bash

set -exo pipefail

API_URL=https://api.github.com/repos/hikhvar/mqtt2prometheus/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.name[1:]) \(.published_at) \(.tarball_url)"'


ROOT=$(pwd)
function fetch() {
    apt install --assume-yes --no-install-recommends git golang-go
    cd "${SRCDIR}"
    mkdir -p "mqtt2prometheus-${VERSION}"
    wget "${URL}" --output-document "mqtt2prometheus-${VERSION}.tar.gz"
    tar -x -C "mqtt2prometheus-${VERSION}" --strip-components=1 -f "mqtt2prometheus-${VERSION}.tar.gz"
    rm "mqtt2prometheus-${VERSION}.tar.gz"
    cd "mqtt2prometheus-${VERSION}"
    make
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/mqtt2prometheus-${VERSION}/bin/mqtt2prometheus.linux_amd64" "${PKGDIR}/usr/bin/mqtt2prometheus"
    cp "${SRCDIR}/mqtt2prometheus-${VERSION}/config.yaml.dist" "${PKGDIR}/etc/mqtt2prometheus.yml"
    cp "${ROOT}/mqtt2prometheus.service" "${PKGDIR}/lib/systemd/system/mqtt2prometheus.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
