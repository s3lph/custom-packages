#!/bin/bash

set -exo pipefail


ROOT=$(pwd)
function fetch() {
    apt install --assume-yes --no-install-recommends git cargo clang pkg-config libssl-dev libglib2.0-dev libnice-dev
    cd "${SRCDIR}"
    git clone https://github.com/johni0702/mumble-web-proxy mumble-web-proxy
    cd mumble-web-proxy
    export VERSION=0.$(git rev-list --count HEAD)
    cargo build --release
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/mumble-web/proxy" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/mumble-web-proxy/target/release/mumble-web-proxy" "${PKGDIR}/usr/bin/mumble-web-proxy"
    cp "${ROOT}/mumble-web-proxy@.service" "${PKGDIR}/lib/systemd/system/mumble-web-proxy@.service"
    cp "${ROOT}/mumble-web-proxy.conf" "${PKGDIR}/etc/mumble-web/proxy/mumble.conf"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
