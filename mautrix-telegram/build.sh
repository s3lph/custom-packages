#!/bin/bash

set -exo pipefail

. ../.skel/helpers.sh

export PKGNAME=mautrix-telegram
API_URL=https://api.github.com/repos/mautrix/telegram/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.name[1:]) \(.published_at) \(.tarball_url)"'

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    mkdir -p "mautrix-telegram-${VERSION}"
    wget "${URL}" --output-document "mautrix-telegram-${VERSION}.tar.gz"
    tar -x -C "mautrix-telegram-${VERSION}" --strip-components=1 -f "mautrix-telegram-${VERSION}.tar.gz"
    rm "mautrix-telegram-${VERSION}.tar.gz"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/etc/default" \
	  "${PKGDIR}/usr/lib/python3/dist-packages" \
          "${PKGDIR}/lib/systemd/system" \
          "${PKGDIR}/usr/share/doc/mautrix-telegram" \
          "${PKGDIR}/var/lib/mautrix-telegram" \
          "${PKGDIR}/var/log/mautrix-telegram"
    cd "${SRCDIR}/mautrix-telegram-${VERSION}"
    python3 setup.py egg_info install --root="${PKGDIR}/" --prefix=/usr --optimize=1
    rsync -a "${PKGDIR}/usr/local/lib/python3.11/dist-packages/" "${PKGDIR}/usr/lib/python3/dist-packages/"
    rm -rf "${PKGDIR}/usr/local/lib/python3.11/"
    find "${PKGDIR}/usr/lib/python3/dist-packages" -name __pycache__ -exec rm -r {} \; 2>/dev/null || true
    find "${PKGDIR}/usr/lib/python3/dist-packages" -name '*.pyc' -exec rm {} \;
    find "${PKGDIR}/usr/lib/python3/dist-packages" -type d -exec chmod 0755 {} \;
    find "${PKGDIR}/usr/lib/python3/dist-packages" -type f -exec chmod 0644 {} \;
    cp "${ROOT}/mautrix-telegram.service" "${PKGDIR}/lib/systemd/system/mautrix-telegram.service"
    cp "${ROOT}/mautrix-telegram.default" "${PKGDIR}/etc/default/mautrix-telegram"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    cp "${PKGDIR}/usr/lib/python3/dist-packages/mautrix_telegram/example-config.yaml" "${PKGDIR}/etc/mautrix-telegram.yml"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s# ./mautrix-telegram.log# /var/log/mautrix-telegram/mautrix-telegram.log#g" -i "${PKGDIR}/etc/mautrix-telegram.yml"
    chmod 0640 "${PKGDIR}/etc/mautrix-telegram.yml"
    install -m 0644 "${SRCDIR}/mautrix-telegram-${VERSION}/LICENSE" "${PKGDIR}/usr/share/doc/${PKGNAME}/copyright"
    github_changelog mautrix/telegram
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
