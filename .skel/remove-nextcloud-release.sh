#!/bin/bash

if [[ "$#" -ne 1 ]]; then
    echo "Usage: $0 <major version>"
    exit 1
fi

if [[ -z "${GITEA_TOKEN}" ]]; then
    echo "Missing env: GITEA_TOKEN"
    exit 1
fi

packages=$(curl -s -H "Authorization: Bearer ${GITEA_TOKEN}" "https://git.kabelsalat.ch/api/v1/packages/s3lph?type=debian&q=nextcloud-$1" | jq -r '.[] | "\(.name)/\(.version)"')

nextgen=$(curl -s -H "Authorization: Bearer ${GITEA_TOKEN}" "https://git.kabelsalat.ch/api/v1/packages/s3lph?type=debian&q=nextcloud-$(($1+1))" | jq -r length)
if [[ "${nextgen}" -le 0 ]]; then
    echo "No packages matching nextcloud-$(($1+1)); refusing deletion of nextcloud-$1"
    exit 2
fi

for package in $packages; do
    echo "$package"
    curl -s -H "Authorization: Bearer ${GITEA_TOKEN}" -X DELETE "https://git.kabelsalat.ch/api/v1/packages/s3lph/debian/$package"
done
