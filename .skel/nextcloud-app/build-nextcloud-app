#!/bin/bash

set -exo pipefail

. $(dirname ${BASH_SOURCE[0]})/../helpers.sh

export ARCHS=(all)


function postinst_configure_pre() { :; }
function postinst_configure_post() { :; }


function _vcompare() {
    for V in $@; do
        if [[ "$V" =~ ^\<[0-9.]*$ ]]; then [[ $(echo -e "$NCMAJOR_OVERRIDE\n${V:1}" | sort -V | head -1) -eq "$NCMAJOR_OVERRIDE" ]] || return  1;
        elif [[ "$V" =~ ^\<=[0-9.]*$ ]]; then [[ $(echo -e "$NCMAJOR_OVERRIDE\n${V:2}" | sort -V | head -1) -eq "$NCMAJOR_OVERRIDE" ]] || return  1;
        elif [[ "$V" =~ ^\>[0-9.]*$ ]]; then [[ $(echo -e "$NCMAJOR_OVERRIDE\n${V:1}" | sort -rV | head -1) -eq "$NCMAJOR_OVERRIDE" ]] || return  1;
        elif [[ "$V" =~ ^\>=[0-9.]*$ ]]; then [[ $(echo -e "$NCMAJOR_OVERRIDE\n${V:2}" | sort -rV | head -1) -eq "$NCMAJOR_OVERRIDE" ]] || return  1;
        elif [[ "$V" =~ ^=[0-9.]*$ ]]; then [[ "$NCMAJOR_OVERRIDE" -eq "${V:1}" ]] || return  1;
        elif [[ "$V" =~ ^==[0-9.]*$ ]]; then [[ "$NCMAJOR_OVERRIDE" -eq "${V:1}" ]] || return  1;
        else
            echo "Unknown version string '${V}'"
            return 1
        fi
    done
    return 0
}


function pre_fetch() { :; }
function _fetch() {
    cd "${SRCDIR}"
    VERSIONS=$(curl -H 'Accept: application/json' "${_NCAPPS_URL}" | jq --arg app "${APP}" -r "${_NCAPPS_JQ}")
    while read -r VERSION URL COMPAT; do
        if _vcompare $COMPAT; then
            echo "Fetching version ${VERSION}"
            wget "${URL}" --output-document "app.tar.gz"
            tar -xf "app.tar.gz"
            return
        fi
    done <<<$VERSIONS
    echo "No compatible app version is available"
    exit 1
}
function post_fetch() { :; }

function pre_prepare() { :; }
function _prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/share/doc/${PKGNAME}" \
          "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps"
    VERSION=$(cat "${SRCDIR}/${APP}/appinfo/info.xml" | xq-python -r .info.version)
    SUMMARY=$(cat "${SRCDIR}/${APP}/appinfo/info.xml" | xq-python -r '.info.summary | if type == "array" then (.[] | select(.["@lang"]=="en") | .["#text"]) else (.) end')
    DESCRIPTION=$(cat "${SRCDIR}/${APP}/appinfo/info.xml" | xq-python -r '.info.description | if type == "array" then (.[] | select(.["@lang"]=="en") | .["#text"]) else (.) end')
    PHP_MIN=$(cat "${SRCDIR}/${APP}/appinfo/info.xml" | xq-python -r '.info.dependencies.php."@min-version"')
    PHP_MAX=$(cat "${SRCDIR}/${APP}/appinfo/info.xml" | xq-python -r '.info.dependencies.php."@max-version"')
    PHP_DEP=""
#    #f [[ "${PHP_MIN}" != "null" ]]; then
#	PHP_DEP="${PHP_DEP}, php (>= ${PHP_MIN})"
#    fi
#    if [[ "${PHP_MAX}" != "null" ]]; then
#	# awk increments the last version component, from https://stackoverflow.com/a/69489163
#	PHP_MAX=$(echo "${PHP_MAX}" | awk -F. -v OFS=. '{$NF=$NF+1;print}')
#	PHP_DEP="${PHP_DEP}, php (<< ${PHP_MAX})"
#    fi
    cp -r "${SRCDIR}/${APP}" "${PKGDIR}/usr/lib/nextcloud/nextcloud-apps/${APP}"
    cat > "${PKGDIR}/DEBIAN/control" <<EOF
Package: ${PKGNAME}
Version: ${VERSION}
Maintainer: ${MAINTAINER}
Section: web
Priority: optional
Architecture: ${ARCH}
Depends: nextcloud-${NCMAJOR}${PHP_DEP}${ADDITIONAL_DEPS}
Provides: nextcloud-app-${APP//_/-}
Conflicts: nextcloud-app-${APP//_/-}
Description: ${SUMMARY}
$(echo ${DESCRIPTION} | fold --width=71 --spaces | sed -re 's/^([^ ])/ \1/g')
EOF
    cat > "${PKGDIR}/DEBIAN/postinst" <<EOF
#!/bin/bash
set -exo pipefail

if [[ "\$1" == "configure" ]]; then

$(declare -f postinst_configure_pre | sed -e '1,2d' -e '$d')

    # Only run migrations if nextcloud is installed and the app is enabled
    if [[ -z "\$(grep installed /var/lib/nextcloud/webroot/config/config.php | grep false)" && \\
          -n \$(sudo -u www-data php /var/lib/nextcloud/webroot/occ app:list | grep -B 10000 '^Disabled:' | grep -- "- ${APP}:") ]]; then
        sudo -u www-data php /var/lib/nextcloud/webroot/occ upgrade
        sudo -u www-data php /var/lib/nextcloud/webroot/occ maintenance:mode --off
    fi

$(declare -f postinst_configure_post | sed -e '1,2d' -e '$d')

fi
EOF
}
function post_prepare() { :; }

function pre_chown() { :; }
function _chown() {
    find "${PKGDIR}" -type f -exec chmod 0644 {} \;
    find "${PKGDIR}" -type d -exec chmod 0755 {} \;
    # root:www-data
    chown 0:0 -R "${PKGDIR}"
    chown 0:33 -R "${PKGDIR}/usr/lib/nextcloud"
    find "${PKGDIR}/DEBIAN" -type f -exec chmod 0644 {} \;
    find "${PKGDIR}/DEBIAN" -type d -exec chmod 0755 {} \;
    chmod -f 0755 "${PKGDIR}/DEBIAN/preinst" "${PKGDIR}/DEBIAN/postinst" "${PKGDIR}/DEBIAN/prerm" "${PKGDIR}/DEBIAN/postrm" || true
    find "${PKGDIR}" -exec touch -m --reference "${SRCDIR}/${APP}/appinfo/info.xml" {} \;
}
function post_chown() { :; }

function pre_package() { :; }
function _package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}
function post_package() { :; }

function build_nextcloud_app() {
    export APP=$1
    export NCMAJOR=$2
    export NCMAJOR_OVERRIDE=${3:-$2}
    export PKGNAME="nextcloud-${NCMAJOR}-app-${APP//_/-}"
    export ROOT=${PWD}
    export SKELDIR=$(dirname ${BASH_SOURCE[0]})
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    mkdir -p ${SRCDIR}
    export _NCAPPS_URL=https://apps.nextcloud.com/api/v1/apps.json
    export _NCAPPS_JQ='.[] | select(.id == $app) | .releases[] | select(.isNightly == false) | select(.version | contains("-") == false) | "\(.version) \(.download) \(.rawPlatformVersionSpec)"'
    pre_fetch
    _fetch
    post_fetch
    for ARCH in ${ARCHS[@]}; do
	export ARCH
	if [[ "${ARCH}" == "all" ]]; then
	    export PKGDIR=${ROOT}/build/pkgdir
	else
	    export PKGDIR=${ROOT}/build/pkgdir-${ARCH}
	fi
	mkdir -p ${PKGDIR}
	pre_prepare
	_prepare
	post_prepare
	pre_chown
	_chown
	post_chown
	pre_package
	_package
	post_package
    done
}
