#!/bin/bash

function github_changelog() {
    curl https://api.github.com/repos/$1/releases | jq -r 'reverse | .[] | select(.draft==false and .prerelease==false) | "'"${PKGNAME}"' (\(.name)); urgency=medium\n  \(.body // empty | gsub("\n";"\n  "))\n -- '"${MAINTAINER}"'  \(.created_at | fromdate | strftime("%a, %d %b %Y %T %z"))\n"' | gzip -9n > "${PKGDIR}/usr/share/doc/${PKGNAME}/changelog.gz"
}

function gitlab_changelog() {
    curl https://gitlab.com/api/v4/$1/releases | jq -r 'reverse | .[] | select(.upcoming_release==false) | "'"${PKGNAME}"' (\(.name)); urgency=medium\n  \(.description // empty | gsub("\n";"\n  "))\n -- '"${MAINTAINER}"'  \(.created_at | fromdate | strftime("%a, %d %b %Y %T %z"))\n"' | gzip -9n > "${PKGDIR}/usr/share/doc/${PKGNAME}/changelog.gz"
}
