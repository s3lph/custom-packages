#!/bin/bash

set -exo pipefail

API_URL=https://code.forgejo.org/api/v1/repos/forgejo/runner/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and (.tag_name|test("^v[0-9.-]+$")) ) | "\(.name[1:]) \(.published_at) \(.assets[] | select(.name|test(".*-linux-amd64.xz$")).browser_download_url )"'

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "forgejo-runner-${VERSION}-linux-amd64.xz"
    xz --decompress "forgejo-runner-${VERSION}-linux-amd64.xz"
}

function prepare() {
    chmod +x "${SRCDIR}/forgejo-runner-${VERSION}-linux-amd64"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/forgejo-runner" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/forgejo-runner-${VERSION}-linux-amd64" "${PKGDIR}/usr/bin/forgejo-runner"
    cp "${ROOT}/forgejo-runner-cli" "${PKGDIR}/usr/bin/forgejo-runner-cli"
    cp "${ROOT}/forgejo-runner.service" "${PKGDIR}/lib/systemd/system/forgejo-runner.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    "${PKGDIR}/usr/bin/forgejo-runner" generate-config > "${PKGDIR}/etc/forgejo-runner/config.yml"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
