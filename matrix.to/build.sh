#!/bin/bash

set -exo pipefail

. ../.skel/helpers.sh

PKGNAME=matrix.to
API_URL=https://api.github.com/repos/matrix-org/matrix.to/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="main" ) | "\(.tag_name) \(.published_at) \(.tarball_url)"'

apt update
apt install --yes nodejs npm

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    mkdir -p "${PKGNAME}-${VERSION}"
    wget "${URL}" --output-document "${PKGNAME}-${VERSION}.tar.gz"
    tar -x -C "${PKGNAME}-${VERSION}" --strip-components=1 -f "${PKGNAME}-${VERSION}.tar.gz"
    rm "${PKGNAME}-${VERSION}.tar.gz"
}

function prepare() {
    cd "${ROOT}"
    npm install yarn
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
	  "${PKGDIR}/usr/share/matrix.to/html/" \
	  "${PKGDIR}/usr/share/doc/matrix.to/"
    cd "${SRCDIR}/${PKGNAME}-${VERSION}"
    "${ROOT}/node_modules/.bin/yarn"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    rsync -a "${SRCDIR}/${PKGNAME}-${VERSION}/index.html" "${PKGDIR}/usr/share/matrix.to/html/index.html"
    rsync -a "${SRCDIR}/${PKGNAME}-${VERSION}/src/" "${PKGDIR}/usr/share/matrix.to/html/src/"
    rsync -a "${SRCDIR}/${PKGNAME}-${VERSION}/css/" "${PKGDIR}/usr/share/matrix.to/html/css/"
    rsync -a "${SRCDIR}/${PKGNAME}-${VERSION}/images/" "${PKGDIR}/usr/share/matrix.to/html/images/"
    github_changelog matrix-org/matrix.to
    chown root:www-data -R "${PKGDIR}/usr/share/matrix.to"
    find "${PKGDIR}/usr/share/matrix.to" -type d -exec chmod 0755 {} \;
    find "${PKGDIR}/usr/share/matrix.to" -type f -exec chmod 0644 {} \;
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
