#!/bin/bash

PKGNAME="repo.s3lph.me-apt-source"

mkdir -p build/pkgdir/DEBIAN
mkdir -p build/pkgdir/etc/apt/sources.list.d
mkdir -p build/pkgdir/usr/share/keyrings
mkdir -p "build/pkgdir/usr/share/doc/${PKGNAME}"

cp gitkabelsalatch-s3lph.sources build/pkgdir/etc/apt/sources.list.d/gitkabelsalatch-s3lph.sources
cp gitkabelsalatch-s3lph.asc build/pkgdir/usr/share/keyrings/gitkabelsalatch-s3lph.asc
cp debian.control build/pkgdir/DEBIAN/control
cp debian.conffiles build/pkgdir/DEBIAN/conffiles
cp debian.changelog "build/pkgdir/usr/share/doc/${PKGNAME}/changelog"
cp debian.copyright "build/pkgdir/usr/share/doc/${PKGNAME}/copyright"

sed -re "s/__PKGNAME__/${PKGNAME}/g" -i build/pkgdir/DEBIAN/control
sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i build/pkgdir/DEBIAN/control
sed -re "s/__PKGNAME__/${PKGNAME}/g" -i "build/pkgdir/usr/share/doc/${PKGNAME}/copyright"
sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "build/pkgdir/usr/share/doc/${PKGNAME}/copyright"
sed -re "s/__PKGNAME__/${PKGNAME}/g" -i "build/pkgdir/usr/share/doc/${PKGNAME}/changelog"
sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "build/pkgdir/usr/share/doc/${PKGNAME}/changelog"
gzip -9n "build/pkgdir/usr/share/doc/${PKGNAME}/changelog"

dpkg-deb --build build/pkgdir build
