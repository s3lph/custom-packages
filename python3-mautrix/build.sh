#!/bin/bash

set -exo pipefail

API_URL=https://api.github.com/repos/mautrix/python/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.name[1:]) \(.published_at) \(.tarball_url)"'


ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    mkdir -p "mautrix-python-${VERSION}"
    wget "${URL}" --output-document "mautrix-python-${VERSION}.tar.gz"
    tar -x -C "mautrix-python-${VERSION}" --strip-components=1 -f "mautrix-python-${VERSION}.tar.gz"
    rm "mautrix-python-${VERSION}.tar.gz"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
	  "${PKGDIR}/usr/lib/python3/dist-packages"
    cd "${SRCDIR}/mautrix-python-${VERSION}"
    python3 setup.py egg_info install --root="${PKGDIR}/" --prefix=/usr --optimize=1
    rsync -a "${PKGDIR}/usr/local/lib/python3.11/dist-packages/" "${PKGDIR}/usr/lib/python3/dist-packages/"
    rm -rf "${PKGDIR}/usr/local/lib/python3.11/"
    find "${PKGDIR}/usr/lib/python3/dist-packages" -name __pycache__ -exec rm -r {} \; 2>/dev/null || true
    find "${PKGDIR}/usr/lib/python3/dist-packages" -name '*.pyc' -exec rm {} \;
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
