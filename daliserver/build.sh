#!/bin/bash

set -exo pipefail

ROOT=$(pwd)

function fetch() {
    git clone https://github.com/onitake/daliserver "${SRCDIR}/daliserver"
}

function prepare() {
    cd "${SRCDIR}/daliserver"
    apt install -y build-essential autoconf
    apt build-dep -y "${SRCDIR}/daliserver"
    autoreconf -i
}

function package() {
    cd "${SRCDIR}/daliserver"
    dpkg-buildpackage
    cp ${SRCDIR}/daliserver_*_amd64.deb ${BUILDDIR}
}

function build() {
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${BUILDDIR}/srcdir
    export PKGDIR=${BUILDDIR}/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
