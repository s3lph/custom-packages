#!/bin/bash

. ../.skel/nextcloud-app/build-nextcloud-app

post_prepare() {
    github_changelog nextcloud/spreed
}

build_nextcloud_app spreed 27

