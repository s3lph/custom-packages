#!/bin/bash

set -exo pipefail

. ../.skel/helpers.sh

MAJOR_VERSION=22
export PKGNAME="keycloak-${MAJOR_VERSION}"

API_URL="https://api.github.com/repos/keycloak/keycloak/releases"
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="main" ) | "\(.name) \(.published_at) \(.assets[] | select( .name|test("keycloak-'${MAJOR_VERSION}'.*.tar.gz$") ).browser_download_url)"'

ROOT=$(pwd)

function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "keycloak-${VERSION}.tar.gz"
    tar -xf "keycloak-${VERSION}.tar.gz"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/opt/" \
          "${PKGDIR}/lib/systemd/system" \
          "${PKGDIR}/etc/apache2/sites-available" \
	  "${PKGDIR}/usr/share/doc/${PKGNAME}"
    cp -r "${SRCDIR}/keycloak-${VERSION}" "${PKGDIR}/opt/keycloak/"
    mv "${PKGDIR}/opt/keycloak/conf" "${PKGDIR}/etc/keycloak/"
    ln -s /etc/keycloak "${PKGDIR}/opt/keycloak/conf"
    mkdir -p \
	  "${PKGDIR}/opt/keycloak/data" \
	  "${PKGDIR}/opt/keycloak/ObjectStore"
    rm "${PKGDIR}/opt/keycloak/bin/kc.bat" \
       "${PKGDIR}/etc/keycloak/README.md"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/keycloak.service" "${PKGDIR}/lib/systemd/system/"
    cp "${ROOT}/keycloak.site.conf" "${PKGDIR}/etc/apache2/sites-available/"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.copyright" "${PKGDIR}/usr/share/doc/${PKGNAME}/copyright"
    github_changelog keycloak/keycloak
    find "${PKGDIR}" -exec touch -m --reference "${SRCDIR}/keycloak-${VERSION}/version.txt" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
