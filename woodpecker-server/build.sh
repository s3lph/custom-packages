#!/bin/bash

set -exo pipefail

API_URL=https://api.github.com/repos/woodpecker-ci/woodpecker/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false ) | "\(.tag_name[1:]) \(.published_at) \(.assets[] | select( .name=="woodpecker-server_linux_amd64.tar.gz" ).browser_download_url)"'

ROOT=$(pwd)
BUILDDIR=${ROOT}/build
SRCDIR=${BUILDDIR}/srcdir
PKGDIR=${BUILDDIR}/pkgdir

function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "woodpecker-server-${VERSION}-linux-amd64.tar.gz"
    tar -xf "woodpecker-server-${VERSION}-linux-amd64.tar.gz"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/default" \
          "${PKGDIR}/var/lib/woodpecker/data" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/woodpecker-server" "${PKGDIR}/usr/bin/woodpecker-server"
    cp "${ROOT}/woodpecker-server.service" "${PKGDIR}/lib/systemd/system/woodpecker-server.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    cp "${ROOT}/woodpecker-server.default" "${PKGDIR}/etc/default/woodpecker-server"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -r "${SRCDIR}/woodpecker-server" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    mkdir -p "${SRCDIR}" "${PKGDIR}"
    fetch
    prepare
    package
}
 

build
