#!/bin/bash

set -exo pipefail

API_URL=https://api.github.com/repos/maubot/maubot/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.name[1:]) \(.published_at) \(.tarball_url)"'

apt update
apt install --yes nodejs npm yarnpkg

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    mkdir -p "maubot-${VERSION}"
    wget "${URL}" --output-document "maubot-${VERSION}.tar.gz"
    tar -x -C "maubot-${VERSION}" --strip-components=1 -f "maubot-${VERSION}.tar.gz"
    rm "maubot-${VERSION}.tar.gz"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/etc/default" \
	  "${PKGDIR}/usr/lib/python3/dist-packages" \
          "${PKGDIR}/lib/systemd/system" \
          "${PKGDIR}/usr/lib/maubot/plugins" \
          "${PKGDIR}/var/lib/maubot/plugins" \
          "${PKGDIR}/var/lib/maubot/trash" \
          "${PKGDIR}/var/log/maubot" \
	  "${PKGDIR}/usr/lib/python3/dist-packages/maubot/management/frontend"
    cd "${SRCDIR}/maubot-${VERSION}"
    python3 setup.py egg_info install --root="${PKGDIR}/" --prefix=/usr --optimize=1
    rsync -a "${PKGDIR}/usr/local/lib/python3.11/dist-packages/" "${PKGDIR}/usr/lib/python3/dist-packages/"
    # Build frontend
    cd maubot/management/frontend
    yarnpkg --prod
    yarnpkg build
    rsync -a "${SRCDIR}/maubot-${VERSION}/maubot/management/frontend/build/" "${PKGDIR}/usr/lib/python3/dist-packages/maubot/management/frontend/build/"
    cd "${SRCDIR}/maubot-${VERSION}"
    # continue package preparation
    rm -rf "${PKGDIR}/usr/local/"
    find "${PKGDIR}/usr/lib/python3/dist-packages" -name __pycache__ -exec rm -r {} \; 2>/dev/null || true
    find "${PKGDIR}/usr/lib/python3/dist-packages" -name '*.pyc' -exec rm {} \;
    cp "${ROOT}/maubot.service" "${PKGDIR}/lib/systemd/system/maubot.service"
    cp "${ROOT}/maubot.default" "${PKGDIR}/etc/default/maubot"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    cp "${PKGDIR}/usr/lib/python3/dist-packages/maubot/example-config.yaml" "${PKGDIR}/etc/maubot.yml"
    find "${PKGDIR}/usr/lib/maubot" "${PKGDIR}/usr/lib/python3/dist-packages" -type d -exec chmod 0755 {} \;
    find "${PKGDIR}/usr/lib/maubot" "${PKGDIR}/usr/lib/python3/dist-packages" -type f -exec chmod 0644 {} \;
    chmod 0640 "${PKGDIR}/etc/maubot.yml"
    # Sensible config defaults
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s# ./maubot.log# /var/log/maubot/maubot.log#g" -i "${PKGDIR}/etc/maubot.yml"
    sed -re "s#upload: ./plugins#upload: /var/lib/maubot/plugins#g" -i "${PKGDIR}/etc/maubot.yml"
    sed -re "s#db: ./plugins#db: /var/lib/maubot/plugins#g" -i "${PKGDIR}/etc/maubot.yml"
    sed -re "s#trash: ./trash#trash: /var/lib/maubot/trash#g" -i "${PKGDIR}/etc/maubot.yml"
    sed -re "s#sqlite: ./plugins#sqlite: /var/lib/maubot/trash#g" -i "${PKGDIR}/etc/maubot.yml"
    sed -re "s#- ./plugins#- /var/lib/maubot/plugins\n    - /usr/lib/maubot/plugins#g" -i "${PKGDIR}/etc/maubot.yml"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
