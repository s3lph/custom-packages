#!/bin/bash

set -exo pipefail

N_RELEASES=1
API_URL=https://api.github.com/repos/Luzilla/dnsbl_exporter/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.name) \(.published_at) \(.assets[] | select( .name|test(".*_Linux_x86_64.tar.gz$") ).browser_download_url)"'

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "dnsbl-exporter-${VERSION}-linux-amd64.tar.gz"
    tar -xf "dnsbl-exporter-${VERSION}-linux-amd64.tar.gz"
}

function prepare() {
    chmod +x "${SRCDIR}/dnsbl-exporter"
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/lib/systemd/system" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/default" \
          "${PKGDIR}/etc/prometheus/dnsbl-exporter"
    cp "${SRCDIR}/dnsbl-exporter" "${PKGDIR}/usr/bin/prometheus-dnsbl-exporter"
    cp "${ROOT}/prometheus-dnsbl-exporter.service" "${PKGDIR}/lib/systemd/system/prometheus-dnsbl-exporter.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/prometheus-dnsbl-exporter.defaults" "${PKGDIR}/etc/default/prometheus-dnsbl-exporter"
    cp "${ROOT}/rbls.ini" "${PKGDIR}/etc/prometheus/dnsbl-exporter/rbls.ini"
    cp "${ROOT}/targets.ini" "${PKGDIR}/etc/prometheus/dnsbl-exporter/targets.ini"
    V="${VERSION/v/}"
    sed -re "s/__VERSION__/${V}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    while read VERSION ISODATE URL; do
        export VERSION
        export ISODATE
        export URL
        export BUILDDIR=${ROOT}/build
        export SRCDIR=${ROOT}/build/${VERSION}/srcdir
        export PKGDIR=${ROOT}/build/${VERSION}/pkgdir
        mkdir -p ${SRCDIR} ${PKGDIR}
        fetch
        prepare
        package
    done <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head "-${N_RELEASES}")
}
 

build
