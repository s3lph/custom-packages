#!/bin/bash

set -exo pipefail

API_URL=https://api.github.com/repos/vector-im/hydrogen-web/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false and .target_commitish=="master" ) | "\(.tag_name[1:]) \(.published_at) \(.assets[] | select(.name|test("hydrogen-web-.*.tar.gz$")).browser_download_url )"'

ROOT=$(pwd)
function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "hydrogen-web-${VERSION}.tar.gz"
    tar -xf "hydrogen-web-${VERSION}.tar.gz"
    rm -f "hydrogen-web-${VERSION}.tar.gz"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/share/matrix-hydrogen" \
          "${PKGDIR}/etc/matrix-hydrogen" \
          "${PKGDIR}/etc/apache2/sites-available"
    cp -r "${SRCDIR}" "${PKGDIR}/usr/share/matrix-hydrogen/html"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/matrix-hydrogen.site.conf" "${PKGDIR}/etc/apache2/sites-available/matrix-hydrogen.site.conf"
    mv "${PKGDIR}/usr/share/matrix-hydrogen/html/config.sample.json" "${PKGDIR}/etc/matrix-hydrogen/config.json"
    ln -s "/etc/matrix-hydrogen/config.json" "${PKGDIR}/usr/share/matrix-hydrogen/html/config.json"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    # root:www-data
    chown 0:33 -R "${PKGDIR}/usr/share/matrix-hydrogen" "${PKGDIR}/etc/matrix-hydrogen"
    find "${PKGDIR}" -exec touch -m -d "${ISODATE}" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    export BUILDDIR=${ROOT}/build
    export SRCDIR=${ROOT}/build/srcdir
    export PKGDIR=${ROOT}/build/pkgdir
    mkdir -p ${SRCDIR} ${PKGDIR}
    fetch
    prepare
    package
}
 

build
