#!/bin/bash

set -exo pipefail

API_URL=https://api.github.com/repos/woodpecker-ci/woodpecker/releases
JQ_EXPR='.[] | select( .prerelease==false and .draft==false ) | "\(.tag_name[1:]) \(.published_at) \(.assets[] | select( .name=="woodpecker-agent_linux_amd64.tar.gz" ).browser_download_url)"'

ROOT=$(pwd)
BUILDDIR=${ROOT}/build
SRCDIR=${BUILDDIR}/srcdir
PKGDIR=${BUILDDIR}/pkgdir

function fetch() {
    cd "${SRCDIR}"
    wget "${URL}" --output-document "woodpecker-agent-${VERSION}-linux-amd64.tar.gz"
    tar -xf "woodpecker-agent-${VERSION}-linux-amd64.tar.gz"
}

function prepare() {
    mkdir -p \
          "${PKGDIR}/DEBIAN" \
          "${PKGDIR}/usr/bin" \
          "${PKGDIR}/etc/default" \
          "${PKGDIR}/etc/woodpecker" \
          "${PKGDIR}/var/lib/woodpecker/data" \
          "${PKGDIR}/lib/systemd/system"
    cp "${SRCDIR}/woodpecker-agent" "${PKGDIR}/usr/bin/woodpecker-agent"
    cp "${ROOT}/woodpecker-agent.service" "${PKGDIR}/lib/systemd/system/woodpecker-agent.service"
    cp "${ROOT}/debian.control" "${PKGDIR}/DEBIAN/control"
    cp "${ROOT}/debian.conffiles" "${PKGDIR}/DEBIAN/conffiles"
    cp "${ROOT}/debian.postinst" "${PKGDIR}/DEBIAN/postinst"
    cp "${ROOT}/debian.prerm" "${PKGDIR}/DEBIAN/prerm"
    cp "${ROOT}/debian.postrm" "${PKGDIR}/DEBIAN/postrm"
    cp "${ROOT}/woodpecker-agent.default" "${PKGDIR}/etc/default/woodpecker-agent"
    cp "${ROOT}/agent.conf" "${PKGDIR}/etc/woodpecker/agent.conf"
    sed -re "s/__VERSION__/${VERSION}/g" -i "${PKGDIR}/DEBIAN/control"
    sed -re "s/__MAINTAINER__/${MAINTAINER}/g" -i "${PKGDIR}/DEBIAN/control"
    find "${PKGDIR}" -exec touch -m -r "${SRCDIR}/woodpecker-agent" {} \;
}

function package() {
    cd "${BUILDDIR}"
    dpkg-deb --build "${PKGDIR}" "${BUILDDIR}"
}

function build() {
    read VERSION ISODATE URL <<<$(curl "${API_URL}" | jq -r "${JQ_EXPR}" | head -1)
    export VERSION
    export ISODATE
    export URL
    mkdir -p "${SRCDIR}" "${PKGDIR}"
    fetch
    prepare
    package
}
 

build
